var a = 10; // automatic inference 
var b = 'xxxxx';
var c = true;
var d = 1.0;
var t;
console.log(typeof (a));
console.log(typeof (b));
console.log(typeof (c));
console.log(typeof (d));
t = 'x';
console.log(typeof (t));
t = 100;
console.log(typeof (t));
