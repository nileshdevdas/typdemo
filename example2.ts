/**
 * What is a function and how to invoke a function
 */

function app1(){
    console.log('this is app1');
}

function app2(a:string , b:number , c : boolean ){
    console.log(a);
    console.log(b);
    console.log(c);
}
app1();
app2('1', 1, true);

