/**
 * What are typescript variables and  data types 
 * What e
 */

let a = 10;  // automatic inference 
let b  = 'xxxxx'; 
let c = true; 
let d = 1.0
let t: any ; 
console.log(typeof(a));
console.log(typeof(b));
console.log(typeof(c));
console.log(typeof(d));
t = 'x';
console.log(typeof(t))
t = 100;
console.log(typeof(t))